"""CLI Tool for `barra_to_tfv`

This tool provides two sub-programs:
- Downloading raw barra data
- Merging raw barra data and assisting with TFV fvc setup

Creator: AEWaterhouse
Last Modified: 31/08/2022
"""
import argparse
from pathlib import Path
from .barra_tools import DownloadBarra, MergeBarra


def dir_path(path):
    if Path(path).is_dir():
        return Path(path)
    else:
        raise argparse.ArgumentTypeError(
            f"--path:{path} is not a valid path - check that it exists"
        )


def run_command(parser, args):
    if args.command == "A":
        xlims = tuple([float(x) for x in args.bbox[:2]])
        ylims = tuple([float(x) for x in args.bbox[2:]])
        print(xlims)
        print(ylims)
        DownloadBarra(
            args.time_start,
            args.time_end,
            xlims,
            ylims,
            args.path,
            args.max_tries,
            args.timeout,
        )
    elif args.command == "B":
        if (args.timezone_offset != None) & (args.timezone_label != None):
            tz = (args.timezone_offset, args.timezone_label)
        elif (args.timezone_offset != None):
            sign = '+' if args.timezone_offset else '-'
            tz = (args.timezone_offset, f"UTC{sign}{args.timezone_offset:0.1f}")
        elif (args.timezone_offset == None) & (args.timezone_label != None):
            assert False, 'Need to supply a timezone_offset!'
        else:
            tz = None

        MergeBarra(
            args.in_path,
            args.out_path,
            args.file_name,
            args.time_start,
            args.time_end,
            args.write_fvc,
            args.reproject,
            tz,
        )
        
        
def cli_get_barra():
    """Main tool entry point
    """
    parser = argparse.ArgumentParser(
        prog="barra_to_tfv.py",
        epilog="See '<command> --help' to read about a specific sub-command.",
    )
    subparsers = parser.add_subparsers(dest="command", help="Sub-commands")

    A_parser = subparsers.add_parser(
        "A",
        help="Download raw BARRA files for a set time period and bounding box extents",
    )
    A_parser.add_argument("time_start", help='Start time in format "yyyy-mm-dd HH:MM"')
    A_parser.add_argument("time_end", help='End time in format "yyyy-mm-dd HH:MM"')
    A_parser.add_argument(
        "bbox", nargs=4, help='Bounding box lon/lat extents as a list "xmin xmax ymin ymax"'
    )
    A_parser.add_argument(
        "--path", default=".", type=dir_path, help="Output directory, needs to exist first"
    )
    A_parser.add_argument(
        "--max_tries",
        type=int,
        help="Number of times to retry a failed BARRA file download",
    )
    A_parser.add_argument(
        "--timeout",
        type=int,
        help="Timeout in seconds between retrying to download a BARRA file",
    )
    A_parser.set_defaults(func=run_command)

    B_parser = subparsers.add_parser(
        "B",
        help="Merge raw BARRA files into a single netcdf, and optionally write a TUFLOW-FV FVC file",
    )
    B_parser.add_argument(
        "--file_name",
        help='Merged BARRA netcdf filename. Default: "BARRA_<time_start>_<time_end>.nc"',
    )
    B_parser.add_argument("--time_start", help='Start time in format "yyyy-mm-dd HH:MM"')
    B_parser.add_argument("--time_end", help='End time in format "yyyy-mm-dd HH:MM"')
    B_parser.add_argument(
        "-inp", "--in_path",
        default=".",
        type=dir_path,
        help="Path to the directory holding the raw data files",
    )
    B_parser.add_argument(
        "-op", "--out_path",
        default=".",
        type=dir_path,
        help="Output directory for the merged dataset, should exist first",
    )
    B_parser.add_argument(
        "-fvc", "--write_fvc",
        default=True,
        help="Write a TUFLOW-FV '.fvc' file to accompany merged dataset",
    )
    B_parser.add_argument(
        "-rp", "--reproject",
        type=int,
        default=None,
        help="Reproject coordinates to a new coord system, input as EPSG code (e.g., 4326)",
    )
    B_parser.add_argument(
        "-tz", "--timezone_offset",
        type=float,
        default=None,
        help='Fixed offset hours for local timezone, e.g. "-tz 10',
    )
    B_parser.add_argument(
        "-ltz", "--timezone_label",
        type=str,
        default=None,
        help='Custom timezone label, e.g. "-ltz AEST',
    )
    ## -- Add EPSG code input
    ## -- Add Local Time input
    ## -- Add Custom Input Config
    ## -- Modify attrs of lat lon
    B_parser.set_defaults(func=run_command)

    args = parser.parse_args()
    if args.command is not None:
        args.func(parser, args)
    else:
        parser.print_help()

if __name__ == "__main__":
    cli_get_barra()