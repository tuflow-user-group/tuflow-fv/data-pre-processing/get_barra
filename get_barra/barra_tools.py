# %%
import re
import sys
import time
from pathlib import Path

import numpy as np
import pandas as pd
import xarray as xr
import yaml
from dask.array import arctan2, hypot
from dask.diagnostics import ProgressBar
from siphon.catalog import TDSCatalog
from tqdm import tqdm
from pyproj import Transformer, CRS

from .custom_conversions import *

# %%
# BASE URL
BASE_URL = "https://dapds00.nci.org.au/thredds/catalog/cj37/BARRA/BARRA_R/v1/forecast"

root = Path(__file__).parent
with open(root / "barra_config.yml") as f:
    cfg = yaml.safe_load(f)

# %%
class DownloadBarra:
    def __init__(
        self,
        time_start: str,
        time_end: str,
        xlims: tuple[float, float],
        ylims: tuple[float, float],
        output_directory: Path = Path("."),
        max_tries: int = 10,
        timeout: int = 30,
    ):
        """Download RAW BARRA netcdf files from their THREDDS server.
        NOTE: BARRA is covered under BoM's Express License - please ensure you have correct permissions to use the data.
        This script will take start and end time, plus a bounding box, and download a gazillion netcdf raw files.
        These can be subsequently merged using `MergeBarra`.

        Args:
            time_start (str): Start time to subset files (str: "YYYY-mm-dd HH:MM")
            time_end (str): End time to subset files (str: "YYYY-mm-dd HH:MM")
            xlims (tuple[float, float]): X-limits in longitude, deg. E.g., (140, 160)
            ylims (tuple[float, float]): Y-limits in latitude, deg. E.g., (-20, -10)
            output_directory (Path, optional): Output folder for BARRA raw files. Must exist prior to call. Defaults to Path(".").
            max_tries (int, optional): Max attempts to download file before throwing in the towel.  Defaults to 10.
            timeout (int, optional): Time in seconds before re-attempting download. Defaults to 30.
        """

        assert (
            type(pd.Timestamp(time_start)) == pd.Timestamp
        ), "Could not parse `time_start` argument. Format is string as 'YYYY-mm-dd HH:MM'"
        assert (
            type(pd.Timestamp(time_end)) == pd.Timestamp
        ), "Could not parse `time_end` argument. Format is string as 'YYYY-mm-dd HH:MM'"
        date_range = pd.date_range(time_start, time_end, freq="6H")

        # Assign output dir (assertion logic assigned to main cli app)
        self.output_dir = output_directory

        # Init TDS_Catalog Spider
        tds_spider = TDS_Spider()

        # Fetch the required variables to be downloaded
        var_list = [(x["barra_var"], x["level"]) for x in cfg.values()]

        # Prefetch urls
        files = dict()
        nfiles_to_dl = 0
        for var, lvl in tqdm(var_list, colour="red", desc="Collecting BARRA urls"):
            for date in tqdm(
                date_range, desc=f"...working on: {var}", colour="blue", leave=False
            ):
                fname, url = tds_spider.get_data_url(var, lvl, date)
                files[fname] = dict(url=url, downloaded=False)

                # Check if file has already been downloaded
                if (output_directory / fname).exists():
                    files[fname]["downloaded"] = True
                else:
                    nfiles_to_dl += 1

        self.nfiles = len(files)
        self.nfiles_to_dl = nfiles_to_dl
        nfiles_already_downloaded = self.nfiles - nfiles_to_dl
        self.files = files
        assert (
            self.nfiles > 0
        ), "No files to extract - check the requested time start and time end"

        # Set spatial bnds
        assert xlims[1] > xlims[0], "xMin should be smaller than xMax (xlims argument)"
        assert ylims[1] > ylims[0], "yMin should be smaller than yMax (ylims argument)"
        self.xlims = slice(xlims[0], xlims[1])
        self.ylims = slice(ylims[0], ylims[1])

        # Print request
        print(f"This request involves collection of {self.nfiles} sub-files")
        if nfiles_already_downloaded > 0:
            print(f"... Skipping {nfiles_already_downloaded} files that already exist")
        print(f"... Downloading {nfiles_to_dl}")
        print("\n")
        print("--------------------")
        print("Confirming Request:")
        print(f"... xLims: {xlims}")
        print(f"... yLims: {ylims}")
        print(f"... Dates: {time_start} to {time_end}")
        print("\n")

        # Assign properties regarding timeout/tries
        self.max_tries = max_tries
        self.timeout = timeout

        if _query_yes_no("Do you want to continue?"):
            self.download_subset()
        else:
            print("--- Finished ---")

    def download_subset(self):
        """Internal function to begin downloading the file list"""
        files = self.files
        # Cut down filelist to downloaded files only
        files = {k: v for k, v in files.items() if v["downloaded"] == False}
        for fname, v in tqdm(
            files.items(), desc="Downloading BARRA raw files", colour="green"
        ):
            if v["downloaded"] == True:
                continue
            else:
                self._download_single_file(fname, v["url"])
        print("--- Finished ---")

    def _download_single_file(self, fname, url):
        """Handle single file download

        Args:
            fname (_type_): filename for downloaded netcdf
            url (_type_): OPENDap url
        """
        ds = xr.open_mfdataset([url])
        ds = ds.sel(longitude=self.xlims, latitude=self.ylims)

        output_file = self.output_dir / fname
        status = _handle_download(ds, output_file, self.max_tries, self.timeout)
        self.files[fname]["downloaded"] = status


def _handle_download(
    ds: xr.Dataset, output_file: Path, tries: int, timeout: int
) -> bool:
    """Retry downloading if encountering issues.
    Not very well tested because the server is stable...

    Args:
        ds (xr.Dataset): The dask dataset from the opendap connection
        output_file (Path): output filename
        tries (int): Current try number
        timeout (int): Timeout in seconds between re-attempting download

    Returns:
        bool: Status of download (True == downloaded)
    """
    if tries == 0:
        print(
            f"File {output_file.name} failed to download after 5 tries, please try again later"
        )
        return False
    try:
        ds.to_netcdf(output_file)
        assert output_file.exists()
        return True
    except:
        print("File download failed - timeout for 30 seconds")
        time.sleep(timeout)
        return _handle_download(ds, output_file, tries - 1)


class TDS_Spider:
    def __init__(self):
        self.BASE_URL = BASE_URL
        self.session_url = None
        self.cat = None

    def get_data_url(self, var: str, lvl: str, date: pd.Timestamp) -> tuple[str, str]:
        """Internal function - get BARRA THREDDS URL

        Args:
            var (str): BARRA variable (e.g., "av_uwnd10m")
            date (pd.Timestamp): Requested export date

        Returns:
            tuple[str, str]: full URL to barra filehandle, and the file_name
        """
        year = date.year
        month = date.month
        datestr = date.strftime("%Y%m%dT%H%MZ")
        section_url = f"{BASE_URL}/{lvl}/{var}/{year}/{month:02}/catalog.xml"
        if self.cat == None:
            self.cat = TDSCatalog(section_url)
        else:
            if self.section_url != section_url:
                self.cat = TDSCatalog(section_url)
        self.section_url = section_url
        cat = self.cat
        fname = [str(x) for x in cat.datasets if datestr in x][0]
        dset = cat.datasets[fname]
        url = dset.access_urls["OPENDAP"]
        return fname, url


def _query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def MergeBarra(
    in_path: Path = Path("."),
    out_path: Path = Path("."),
    fname: str = None,
    time_start: str = None,
    time_end: str = None,
    write_fvc=True,
    reproject:int=None,
    local_tz:tuple[float, str]=None,
):
    """Merge raw downloaded BARRA datafiles into a single netcdf file, ready for TUFLOW-FV modelling
    Optionally create an accompanying .fvc file.

    Args:
        in_path (Path, optional): Directory of the raw BARRA data-files. Defaults to Path(".").
        out_path (Path, optional): Output directory for the merged BARRA netcdf and (opt) the fvc. Defaults to Path(".").
        fname (str, optional): Merged BARRA netcdf filename. Defaults to None.
        time_start (str, optional): Start time limit of the merged dataset (str: "YYYY-mm-dd HH:MM"). Defaults to None.
        time_end (str, optional): End time limit of the merged dataset (str: "YYYY-mm-dd HH:MM"). Defaults to None.
        write_fvc (bool, optional): Optionally write an accompanying .fvc file. Defaults to True.
    """
    # Time indexing - simply conversion and assert check
    if time_start:
        assert (
            type(pd.Timestamp(time_start)) == pd.Timestamp
        ), "Could not parse `time_start` argument. Format is string as 'YYYY-mm-dd HH:MM'"
        ts = pd.Timestamp(time_start)
    else:
        ts = None

    if time_end:
        assert (
            type(pd.Timestamp(time_end)) == pd.Timestamp
        ), "Could not parse `time_end` argument. Format is string as 'YYYY-mm-dd HH:MM'"
        te = pd.Timestamp(time_end)
    else:
        te = None
    time_slice = slice(ts, te)

    print("Opening BARRA raw files and preparing to interpolate all to a common grid")
    print("--- This step can take awhile, please wait --- ")
    var_list = [x["barra_var"] for x in cfg.values()]
    ds_set = []
    for var in var_list:
        # var_query = f'{in_path.as_posix()}/{var}*.nc'
        files = list(in_path.glob(f"{var}*.nc"))
        assert (
            len(files) > 0
        ), f"Can't find any raw files for `{var}` at path `{in_path.absolute().as_posix()}`"
        ds = xr.open_mfdataset(files)
        ds = ds.sel(time=time_slice)

        if var == "av_mslp":  # grid definition -> ToDO: Update to be more generic
            lat = ds["latitude"].values
            lon = ds["longitude"].values
            time = ds['time_bnds'][:, 0].values
        
        ds = ds.interp(coords=dict(latitude=lat, longitude=lon, time=time), kwargs={"fill_value": "extrapolate"})

        if "height" in ds.coords:
            ds = ds.drop("height")
        ds = ds[[var]]
        ds_set.append(ds)

    dsx = xr.merge(ds_set)
    # Checks and filling
    dsx = dsx.bfill(dim="latitude")
    dsx = dsx.bfill(dim="longitude")
    dsx = dsx.ffill(dim="latitude")
    dsx = dsx.ffill(dim="longitude")
    
    # # add Wind Mag and Direction, for mad convenience
    # dsx["wind_magnitude"] = hypot(dsx["av_uwnd10m"], dsx["av_vwnd10m"])
    # dsx["wind_magnitude"].attrs = dict(
    #     standard_name="wind_speed", long_name="wind_speed", units="m s-1"
    # )
    # dsx["wind_direction"] = (
    #     270 - 180 / np.pi * arctan2(dsx["av_vwnd10m"], dsx["av_uwnd10m"])
    # ) % 360
    # dsx["wind_direction"].attrs = dict(
    #     standard_name="wind_from_direction", long_name="wind_direction", units="degree"
    # )

    # Rechunk before doing calcs
    dsx = dsx.chunk(dict(time=1))

    # Apply custom functions
    # This is a bit of string magic to allow custom variables to be specifed in the config
    for vals in cfg.values():
        if "pre_process" in vals:
            var = vals["barra_var"]
            components = re.findall("(\D+)\((\D+), (\D+)\)", vals["pre_process"])[0]
            fn_call = eval(components[0])
            in_vars =  [dsx[v] for v in components[1:]]
            dsx[var] = xr.apply_ufunc(fn_call, *in_vars, dask='allowed')

    # Rename variables
    # Use yaml config to map barra var to outname var
    name_remap = {b["barra_var"]: name for name, b in cfg.items()}
    dsx = dsx.rename(name_remap)
    
    # Correctly label lat and lon variables
    wgs84 = CRS.from_epsg(4326)
    dsx['longitude'].attrs = dict(wgs84.cs_to_cf()[0], epsg=4326, name=wgs84.name)
    dsx['latitude'].attrs = dict(wgs84.cs_to_cf()[1], epsg=4326, name=wgs84.name)
    if reproject: # Reproject if necessary
        crs = CRS.from_epsg(reproject)
        transformer = Transformer.from_crs("epsg:4326", crs, always_xy=True)
        
        xv, yv = np.meshgrid(dsx.longitude, dsx.latitude)
        xp, yp = transformer.transform(xv, yv)
        dsx = dsx.assign_coords(dict(
            x=(('latitude', 'longitude'), xp),
            y=(('latitude', 'longitude'), yp)
        ))
        dsx['x'].attrs=dict(crs.cs_to_cf()[0], epsg=reproject, name=crs.name)
        dsx['y'].attrs=dict(crs.cs_to_cf()[1], epsg=reproject, name=crs.name)
        reproject_flag = True
    else:
        reproject_flag = False

    # Prepare time vector for output, and add timezone
    encoding = {"time": {"units": "hours since 1990-01-01 00:00:00"}}
    dsx['time'].attrs = {"tz": "UTC"}
    if local_tz:
        dt = local_tz[0]
        lbl = local_tz[1]
        dsx = dsx.assign_coords(dict(local_time=dsx['time'] + pd.Timedelta(dt, unit='h')))
        dsx['local_time'].attrs = {"tz": lbl}
        encoding['local_time'] = encoding['time']
        local_time_flag = True
    else:
        local_time_flag = False
        
    # Relabel long_names for QGIS
    for var in cfg.keys():
        ln = cfg[var]['long_name']
        dsx[var].attrs['long_name'] = ln

    # Export
    if fname == None:
        ts = pd.Timestamp(dsx["time"][0].values).strftime("%Y%m%d")
        te = pd.Timestamp(dsx["time"][-1].values).strftime("%Y%m%d")
        fname = f"BARRA_{ts}_{te}.nc"

    write_job = dsx.to_netcdf(
        out_path / fname,
        compute=False,
        unlimited_dims="time",
        encoding=encoding,
    )
    with ProgressBar():
        print(f"Begin writing merged netcdf - please wait")
        write_job.compute()

    # Write accompanying FVC control file
    if write_fvc:
        write_tuflowfv_fvc(dsx, fname, out_path, reproject=reproject_flag, local_time=local_time_flag)
    print("--- All Finished --- ")


def write_tuflowfv_fvc(ds: xr.Dataset, fname: str, out_path: Path, reproject=False, local_time=False):
    """Write a tuflow-fv .fvc file to accompany the merged dataset

    Args:
        ds (xr.Dataset): merged barra dataset to supply headers
        fname (str): the filename of the .nc merged dataset
        out_path (Path): output directory
    """
    
    # Check to see if ds has been reprojected. Use x/y if so. 
    if reproject:
        xvar = "x"
        yvar = "y"
    else:
        xvar = "longitude"
        yvar = "latitude"
    
    # Check to see if local_time has been added - use local_time if so.
    if local_time:
        time = "local_time"
    else:
        time = "time"
    
    xlims = ", ".join([f"{fn(ds[xvar].values):0.4f}" for fn in [np.min, np.max]])
    ylims = ", ".join([f"{fn(ds[yvar].values):0.4f}" for fn in [np.min, np.max]])
    # ylims = ", ".join([f"{x:0.4f}" for x in ds[yvar].values[[0, -1]]])

    nc_path = (out_path / fname).as_posix()
    fname_fvc = fname.replace(".nc", ".fvc")
    with open(out_path / fname_fvc, "w") as f:
        f.write("!TUFLOW-FV FVC File for BARRA Atmospheric Dataset\n")
        f.write("!Written by `get_barra.py`\n")
        f.write("\n")
        f.write(f"!Barra netcdf start time: {ds_time_to_str(ds, 0)}\n")
        f.write(f"!Barra netcdf end time: {ds_time_to_str(ds, -1)}\n")
        f.write(f"!Barra netcdf x-limits: {xlims}\n")
        f.write(f"!Barra netcdf y-limits: {ylims}\n")
        f.write("\n")

        # Grid def block
        f.write(f"grid definition file == {nc_path}\n")
        f.write(f"  grid definition variables == {xvar}, {yvar}\n")
        f.write("  grid definition label == barra\n")
        f.write("end grid\n")
        f.write("\n")
        wind_written = False
        for name, itms in cfg.items():
            tfv = itms["tfv_var"]
            bc_scl = itms["bc_scale"]
            bc_off = itms["bc_offset"]

            if (tfv == "W10_GRID") & (wind_written == False):
                f.write(f"bc == {tfv}, barra, {nc_path}\n")
                f.write(f"  bc header == {time}, uwnd10m, vwnd10m\n")
                wind_written = True
            elif (tfv == "W10_GRID") & (wind_written == True):
                continue
            else:
                f.write(f"bc == {tfv}, barra, {nc_path}\n")
                f.write(f"  bc header == {time}, {name}\n")
            f.write("  bc update dt == 3600.\n")
            f.write("  bc time units == hours\n")
            f.write("  bc reference time == 01/01/1990 00:00\n")
            f.write("  bc default == NaN\n")

            # Handle custom scaling (e.g., K to C)
            # Only show if it is relevant (i.e., != 1.0 scl or != 0 off)
            if bc_scl != 1:
                if tfv == "W10_GRID":
                    f.write(f"  bc scale == {bc_scl}, {bc_scl}\n")
                else:
                    f.write(f"  bc scale == {bc_scl}\n")

            if bc_off != 0:
                if tfv == "W10_GRID":
                    f.write(f"  bc offset == {bc_off}, {bc_off}\n")
                else:
                    f.write(f"  bc offset == {bc_off}\n")

            f.write("end bc\n")
            f.write("\n")


def ds_time_to_str(ds: xr.Dataset, i: int, fmt="%Y-%m-%d %H:%M") -> str:
    return pd.Timestamp(ds.time[i].values).strftime(fmt)


# %%
